#!/bin/bash

cd $(dirname $0)/../app

if [ -z $1 ]
then
    echo "Usage: $0 {stocks_to_pick}"
    echo ""
    echo "stocks_to_pick - a number of stocks to pick from the list"
    exit
fi
go run main.go globals.go --stocks_to_pick=$1

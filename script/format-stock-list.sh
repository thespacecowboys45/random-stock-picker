#!/bin/bash
#
# @date 2019.08.11
# @author The Space Cowboy
#
# Description: Add double-quotes and a comma to every line in a file
function usage() {
    echo "Usage: $0 {filename}"
    exit
}

FILENAME=$1
if [ -z ${FILENAME} ]
then
    echo "No filename specified."
    usage
fi

sed -i 's/.*/"&",/g' $FILENAME
echo "Complete!"
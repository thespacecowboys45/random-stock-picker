package main
/*
 * @date 2019.08.11
 * @author The Space Cowboy
 *
 * Description: Pick a random stock from a list
 */
import (
	"fmt"
	"math/rand"
	"time"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
    nStocksToPick = kingpin.Flag("stocks_to_pick", "Number of stocks to pick").Envar("STOCKS_TO_PICK").Default("10").Int()
)

func printFlags() {
    fmt.Printf("nStocksToPick=%d\n", *nStocksToPick)
}

func main() {
	fmt.Printf("Random-Stock-Picker\n")
	
	kingpin.Parse()
	printFlags()
	
	// seed the generator, otherwise you will get the same "random" picks every time you run
	rand.Seed(time.Now().UnixNano())
	nItems := len(stocks)
	
	fmt.Printf("Pick %d stocks from a total of %d.\n", *nStocksToPick, nItems)
	
	for i := 0; i < *nStocksToPick; i++ {
		n := int64(rand.Intn(nItems))
		fmt.Printf("[%d] Choose Item: %d, %s\n", i, n, stocks[n])
	}
	
}
